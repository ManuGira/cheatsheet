## Install and upgrade pip3
	sudo apt install python3-pip	*install pip3*
	pip3 install --upgrade pip	*upgrade pip3 and install pip*

## Install package for **Python 3**
	sudo pip3 install matplotlib
	sudo pip3 install numpy
	sudo pip3 install pyserial		*import serial*
	sudo pip3 install ntplib
	sudo pip3 install jupyter		*To run .ipynb files (ipython inside)*
	sudo pip3 install scipy
	sudo pipt install cvxpy
	sudo apt-get install python3-tk		*import tkinter*
	
* if PermissionError: [Errno 13], use instead **sudo** in **/tmp**
	

## Run **Python 3** in terminal
	python3

# Virtual environnements
Let's use virtualenvwrapper: https://virtualenvwrapper.readthedocs.io/en/latest/.

installation:  
 	`sudo pip install virtualenvwrapper`	*Need to be installed for __python2__*

create a folder for environnements  
 	```    
	export WORKON_HOME=~/Envs  
	mkdir -p $WORKON_HOME  
	source /usr/local/bin/virtualenvwrapper.sh  
	```
	
create new environnement  
	`mkvirtualenv <env_name>`
	
## Usefull commands
	workon <env_name>	#activate environnement
	lsvirtualenv		#list envs
	cpvirtualenv
	rmvirtualenv	

## Kill python process from terminal
* Find python PID:  
	`$ pidof python3.5`

* Verify it correspond to the process you want to kill (second element of the following output)  
	`$ ps aux | grep python3.5`

* Kill the process  
	`$ kill -9 $(pidof python3.5)`
	





