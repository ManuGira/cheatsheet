UNITY3D Cheatsheet

# Project set up
 - In asset folder, create new folders Scene, Scripts and Sprites
 - Save the scene in the *Scene folder*
	


# Spritesheet
## Import/Create sprite
 - Copy Paste the sprite into the Sprites folder
 - In project view, go in the sprite folder and click on the sprite, which open the Inspectors view on the right
 - WARNING: Note that you can�t edit a sprite which is in the Scene View
 - Make sure **TextureType** is set to "2D and ui" and **Sprite Mode to** "Multiple"
 - Open the **Sprite Editor** then click on **Slice** en haut a gauche
 - Set **Type** to "Grid by Cell Count", set the everithing and click on **Slice**
 - More info https://docs.unity3d.com/Manual/SpriteEditor.html
 
## Create an animation:
 - Open the animation tab (Window -> Animation)
 - In the hierachy, click on the desired GameObject to select it
 - In the animation tab and create a new clip.
 - In Asset, select the images needed for the animation and drag n drop them to the animation tab.
 - Set the **sample** value to the desired frame rate.
 - To create a new animation for the same character/ennemy/anything, click on the name of the current animation just under the *record* icon (red circle) and then click on **Create New Clip...**
 
## Use the **Animator Window**
 - Open it: Window -> Animator
 - To make an animation default rightclick -> *Set as default layer state* (it should turn orange)
