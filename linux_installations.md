# apt-get
## Maintaining apt-get:
	apt-get dist-upgrade			*will install or remove package as necessary to complete the upgrade*
	apt full-upgrade				*same as above*
	apt upgrade 					*will automatically install but not remove packages*
	apt update						*needed after installing ppa*

## Usefull commands:
	sudo apt install <package>			*Install a package*
	sudo apt remove <package>			*Uninstall package*

	sudo add-apt-repository ppa:<ppa>		*add a PPA repository*
	sudo add-apt-repository --remove ppa:<ppa>	*Remove a PPA repo*
	sudo ppa-purge <ppa>				*Safe ppa remove (need ppa-purge to be installed)*

	sudo apt-cache search <key_word>		*Search package. Also visit http://packages.ubuntu.com*
	sudo apt autoremove				*Remove no longer required package*
	

## Liste de package installé avec **sudo apt install <package>**:
	ubuntu-make 	*umake* 	*ppa:ubuntu-desktop/ubuntu-make*
	git
	python3-pip	*pip3*
	python3-tk	*import tkinter*
	xclip
	texlive-full	*LaTex*
	octave
	sublime-text			*ppa:webupd8team/sublime-text-2*
	

## Liste des PPA installé avec **sudo add-apt-repository ppa:<ppa>**:
	grep ^[^#] /etc/apt/sources.list /etc/apt/sources.list.d/*

	ubuntu-desktop/ubuntu-make	*umake*
	eugenesan/ppa			*smartgit, uninstalled now*
	webupd8team/sublime-text-2	*sublime-text-2*
	
# Other command frequetly used for installation
	tar -xvzf compressed_folder.tar.gz	*extract folder in current location*


# GitKraken
## Télécharger le debian et l'installer
	wget https://release.gitkraken.com/linux/gitkraken-amd64.deb
	dpkg -i gitkraken-amd64.deb


# SmartGit
## Installer smartgit:
	sudo add-apt-repository ppa:eugenesan/ppa
	sudo apt-get update
	sudo apt-get install smartgit
	

# umake
## Installer umake:
	sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make
	sudo apt-get update
	sudo apt-get install ubuntu-make

## installé avec umake:
	umake ide pycharm	*JetBrain pycharm community edition*
	umake android		*JetBrain android studio*

## désinstaller pycharm (installé avec umake):
	umake -r ide pycharm


# SSH
## Check if system already have a ssh key, try these:
	cat ~/.ssh/id_rsa.pub
	cat ~/.ssh/id_dsa.pub
	cat ~/.ssh/id_ecdsa.pub
	cat ~/.ssh/id_ed25519.pub

## Generate a new ssh key pair:
	ssh-keygen -t rsa -C "your.email@example.com" -b 4096

## Copy your public SSH key to the clipboard:
	xclip -sel clip < ~/.ssh/id_rsa.pub

## Finally paste the key and give it a name related to computer and session
## Test installation with:
	ssh -T git@gitlab.forge.hefr.ch:ubiment/Acquisition_Systems.git		*Wasnt working last time*
	
